/*
 * KeyboardHandler.cpp
 *
 *  Created on: 29 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "KeyboardHandler.h"

#include <iostream>
#include <boost/algorithm/string.hpp>

KeyboardHandler* KeyboardHandler::m_pInstance = NULL;
string const KeyboardHandler::stdPrompt = "\n Type quit to exit" ;
string const KeyboardHandler::quitCmd = "quit";
string KeyboardHandler::promptCommands = "";

KeyboardHandler* KeyboardHandler::getInstance() {
	if ( !m_pInstance ){
		m_pInstance = new KeyboardHandler;
	}
	return m_pInstance;
}

string KeyboardHandler::readString(){
	string result;
	getline(cin, result);
	return result;
}

void KeyboardHandler::operator ()(){
	string  textInput = "";
	RetailShop* shop = new RetailShop();
	this->addCommand("help", [](RetailShop* shop){
		cout <<"Available commands:" <<endl;
		cout << promptCommands <<endl;
	});
	for (auto it = m_pInstance->commands.begin(); it != m_pInstance->commands.end(); ++it){
		string key = it->first;
		promptCommands.append("\t").append(key).append("\n");
	}

	while( ! boost::iequals(textInput, quitCmd))
	{
		cout<<stdPrompt<<endl;
		textInput = readString();
		unordered_map<string, function<void(RetailShop*)> >::iterator got = m_pInstance->commands.find (textInput);

		if ( got != m_pInstance->commands.end() ){
			function<void(RetailShop*)> f = got->second;
			f(shop);
		}
	}


}
void KeyboardHandler::addCommand( string cmdId, function<void(RetailShop*)> f ){
	m_pInstance->commands.insert( make_pair(cmdId, f));
}
