/*
 * RetailShop.cpp
 *
 *  Created on: 19 May 2018
 *      Author: martinayusuf
 */
#include "stdafx.h"
#include "RetailShop.h"

#include "Interfaces/ICustomerAccountManager.h"
#include "CustomerAccountManagerFactory.h"
#include "Warehouse.h"
#include "Domain/Customer.h"
#include "Domain/Item.h"
#include "Domain/Order.h"
#include "Domain/Product.h"
#include "Output/Writer.h"
#include "Input/Reader.h"
#include "Input/SerializedFileReader.h"
#include "Interfaces/BusinessObjectRenderer.h"
#include "Exceptions/InvalidPurchaseException.h"

void RetailShop::bootstrap_products_items(){
	Warehouse::getInstance();
}

void RetailShop::test_serialization_writer_all_products(){
	//++ todo
	// Add some code here to get a seriable writer to write to a file
	//SerializedFileWriter* sfw = ...
	
	unordered_map<string,Product*>  map = Warehouse::getInstance()->getProducts();
	
	//++ todo
	// Uncomment the lines below once you have SerializedFileWriter
	//sfw->writeSerializedMap(map);
	//sfw->close();
}

void RetailShop::test_serialization_reader_all_products(){
	SerializedFileReader* sfr = Reader::getSerializedReader("allProducts.ser");
	unordered_map< string, Product*> productsIn;
	string key;
	Product* prod = new Product();
	productsIn = sfr->readSerializedMap(productsIn, key, prod);
	sfr->close();
	if (productsIn.size() != 0)
	{
		// Iterate over all the Products...
		for (auto it = productsIn.begin(); it != productsIn.end(); ++it)
		{
			Product* pp = it->second;
			cout <<"Product ==> "<< pp->getDescription() <<" " << pp->getPrice() << " " << pp->getProductCode() << " " << pp->getQuantity() << endl;
			// Iterate over all the Items within each Product
			for (auto itt = pp->getItems().begin(); itt != pp->getItems().end(); ++itt)
			{
				Item* anItem = itt->second;
				cout << "Item ==> "<< anItem->getItemCost() <<" " << anItem->getUniqueId() << endl;
			}
		}
	}
}

void RetailShop::bootstrap_customers_account(){
	auto_ptr<CustomerAccountManagerFactory> fp( new CustomerAccountManagerFactory );
	ICustomerAccountManager* caMgr = fp->getCustomerAccountMgr();
	// Create the accounts first and Customers
	Account* acc;
	Customer* cc;

	cc = new Customer("Sevlyn", "Wright", "Somewhere in Birmingham", time(0));
	caMgr->addCustomer("C1", cc);
	acc = new Account(cc, 1);
	cc->setItsAccount(acc); // tie acc to the customer
	caMgr->addAccount(acc);

	cc = new Customer("Samuel", "Wright", "Somewhere else in Birmingham", time(0));
	caMgr->addCustomer("C2", cc);
	acc = new Account(cc, 2);
	cc->setItsAccount(acc); // tie acc to the customer
	caMgr->addAccount(acc);

	cc = new Customer("Graham", "Meaden", "Somewhere down South", time(0));
	caMgr->addCustomer("C3", cc);
	acc = new Account(cc, 3);
	cc->setItsAccount(acc); // tie acc to the customer
	caMgr->addAccount(acc);
}

void RetailShop::test_serialization_writer_all_customers(){
	string fname = "customers.ser";
	//++ todo
	// Add some code here to get a seriable writer to write to a file
	//SerializedFileWriter* sfw = ...
	auto_ptr<CustomerAccountManagerFactory> fp(new CustomerAccountManagerFactory);
	ICustomerAccountManager* caMgr = fp->getCustomerAccountMgr();
	unordered_map <string, Customer*> customers = caMgr->getCustomers();

	//++ todo
	// Uncomment the lines below once you have SerializedFileWriter
	//sfw->writeSerializedMap(customers);
	//sfw->close();
}

void RetailShop::test_serialization_reader_all_customers(){
	SerializedFileReader* sfr = Reader::getSerializedReader("customers.ser");
	unordered_map< string, Customer*> customersIn;
	string key;
	Customer* cust = new Customer();
	customersIn = sfr->readSerializedMap(customersIn, key, cust);
	sfr->close();
	if (customersIn.size() != 0)
	{
		boost::any obj;
		for (auto it = customersIn.begin(); it != customersIn.end(); ++it)
		{
			Customer* cust = it->second;
			obj = cust;
			cust->getRenderer()->display(obj);
			Account* acc = cust->getItsAccount();
			cout << "Account ==> "<< acc->getAccountId()<<" " << endl;
		}
	}
}


void RetailShop::bootstrap_orders_accounts_items(){
	// Get an account and a product
	auto_ptr<CustomerAccountManagerFactory> fp(new CustomerAccountManagerFactory);
	ICustomerAccountManager* caMgr = fp->getCustomerAccountMgr();
	Account* acc = caMgr->getAccount(1);
	Product* pp = Warehouse::getInstance()->getProduct("JDF-001");
	// only proceed if the keys were valid
	if (acc != NULL && pp != NULL)
	{
		try
		{
			// create new order giving it the order number of 1
			Order* oo = new Order();
			oo->setOrderNo(1);
			oo->setOrderDate(time(0));
			// add the order the account

			// Use the product to purchase X times the Product
			Item* itm;
			try{
				itm = pp->purchaseItem(3);
			}
			catch(InvalidPurchaseException* e ){
				cout << e->what() << endl;
			}

			// Only proceed if you used a valid item id
			if (itm != NULL)
			{
				oo->addItem(itm);  // this call decrease the stock for this product
				acc->addOrder(oo);
			}
		} catch (exception& ex)
		{
			cout << ex.what() <<endl;
		}
	}
}

void RetailShop::test_serialization_writer_all_items(){
	//++ todo
	// Add some code here to get a seriable writer to write to a file
	//SerializedFileWriter* sfw = ...
	auto_ptr<CustomerAccountManagerFactory> fp(new CustomerAccountManagerFactory);
	ICustomerAccountManager* caMgr = fp->getCustomerAccountMgr();
	unordered_map<string, Customer*> customers = caMgr->getCustomers();

	//++ todo
	// Uncomment the lines below once you have SerializedFileWriter
	//sfw->writeSerializedMap(customers);
	//sfw->close();
}

void RetailShop::test_serialization_reader_all_items(){
	SerializedFileReader* sfr = Reader::getSerializedReader("accounts.ser");
	unordered_map<string, Customer*> customersIn;
	string key;
	Customer* cust = new Customer();
	customersIn = sfr->readSerializedMap(customersIn, key, cust);
	sfr->close();
	if (customersIn.size() != 0)
	{
		boost::any obj;
		boost::any obj2;
		for (auto it = customersIn.begin(); it != customersIn.end(); ++it)
		{
			Customer* cc = it->second;
			obj = cc;
			cout << "Customer code ==> " << it->first <<endl;
			cc->getRenderer()->display(obj);
			Account* acc = cc->getItsAccount();
			obj2 = acc;
			acc->getRenderer()->display(obj2);
			displayOrderDetails( cc->getItsAccount() );
		}
	}
}

void   RetailShop::displayOrderDetails( Account* acc ){
	unordered_map< int, Order* > orders = acc->getOrders();
	for (auto it = orders.begin(); it != orders.end(); ++it)
	{
		Order* ord = it->second;
		time_t date = ord->getOrderDate();
		char* date_char = ctime(&date);
		cout << "\t" << acc->getAccountId()  << endl;
		cout<<"Order\tORDER NO\tTOTAL COST\tORDER DATE"<<endl;
		cout<<"\t"<< ord->getOrderNo() << "\t\t" << ord->getTotalCost() << "\t\t" <<  date_char <<endl;
		displayItemDetails( ord );
	}
}

void    RetailShop::displayItemDetails( Order* ord )
{
	Item* item = ord->getItems();
	cout<< "Item\tUNIQUE ID\tQUANTITY\tCOST" <<endl;
	cout <<"\t" << item->getUniqueId()<<"\t\t"<<item->getQuantity()<<"\t\t"<<item->getItemCost()<<endl;
	displayProductDetails(item->getItsProduct());
}

void    RetailShop::displayProductDetails( Product* pp )
{

	cout << "Product\tPRODUCT CODE\tDESCRIPTION\t\tPRICE\tQUANTITY" <<endl;
	cout<< "\t"<< pp->getProductCode() << "\t\t" << pp->getDescription()<< "\t\t" << pp->getPrice()<< "\t\t"<<pp->getQuantity()<<endl;
}

