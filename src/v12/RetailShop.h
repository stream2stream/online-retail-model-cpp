/*
 * RetailShop.h
 *
 *  Created on: 19 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_RETAILSHOP_H_
#define V12_RETAILSHOP_H_
#include "Domain/Account.h"
#include "Domain/Product.h"

class RetailShop {
private:
	void displayOrderDetails(Account*);
	void displayItemDetails(Order*);
	void displayProductDetails( Product* );
public:
	RetailShop(){};
	virtual ~RetailShop(){};
	void bootstrap_products_items();
	void bootstrap_customers_account();
	void bootstrap_orders_accounts_items();
	void test_serialization_writer_all_products();
	void test_serialization_reader_all_products();
	void test_serialization_writer_all_customers();
	void test_serialization_reader_all_customers();
	void test_serialization_writer_all_items();
	void test_serialization_reader_all_items();
};

#endif /* V12_RETAILSHOP_H_ */
