/*
 * ShutdownHandler.cpp
 *
 *  Created on: 29 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "ShutdownHandler.h"

#include <thread>
#include "KeyboardHandler.h"

using namespace std;

void ShutdownHandler::waitForShutdownCommand(){
	thread threadObj( (KeyboardHandler()) );
	threadObj.join();
}

