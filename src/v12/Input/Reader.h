/*
 * Reader.h
 *
 *  Created on: 21 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_INPUT_READER_H_
#define V12_INPUT_READER_H_
#include "SerializedFileReader.h"
#include "TextFileReader.h"
class Reader {
private:
	static TextFileReader* fileReader;
	static SerializedFileReader* serializedReader;
public:
	Reader(){};
	static InputChannel* getFileReader( string );
	static SerializedFileReader* getSerializedReader( string );
	virtual ~Reader(){};
};

#endif /* MAIN_CPP_S2S_ONLINE_GARDENING_V09_INPUT_READER_H_ */
