/*
 * SerializedFileReader.h
 *
 *  Created on: 19 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_INPUT_SERIALIZEDFILEREADER_H_
#define V12_INPUT_SERIALIZEDFILEREADER_H_
#include <boost/archive/text_iarchive.hpp>

#include <string>

#include "../Domain/Product.h"
#include "TextFileReader.h"

class SerializedFileReader : public TextFileReader{
public:

	SerializedFileReader(){};
	virtual ~SerializedFileReader(){};

	template <class T>
	T readSerializedData(T obj){
		boost::archive::text_iarchive ia(itsFileReader);
		ia >> obj;
		return obj;
	}

	template <class T, class K, class V>
	T readSerializedMap(T obj, K key, V value){
		boost::archive::text_iarchive ia(itsFileReader);
		int size;
		ia >> size;
		for (size_t i = 0; i != size; ++i) {
		        K key;
		        V value;
		        ia >> key >> value;
		        obj[key] = value;
		    }
		return obj;
	}
};

#endif /* V12_INPUT_SERIALIZEDFILEREADER_H_ */
