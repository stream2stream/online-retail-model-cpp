/*
 * Reader.cpp
 *
 *  Created on: 21 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "Reader.h"

TextFileReader*  Reader::fileReader = new TextFileReader();
SerializedFileReader*  Reader::serializedReader = new SerializedFileReader();

InputChannel* Reader::getFileReader( string fname){
	Reader::fileReader->openForReading(fname);
	return Reader::fileReader;
}

SerializedFileReader* Reader::getSerializedReader( string fname){
	Reader::serializedReader->openForReading(fname);
	return Reader::serializedReader;
}
