/*
 * TextFileReader.cpp
 *
 *  Created on: 15 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "TextFileReader.h"

void TextFileReader::close() {
	try{
	this->itsFileReader.close();
	}
	catch (ios_base::failure& e)
	{
		cout << e.what() << endl;
	}
}

void TextFileReader::openForReading( string fname ){
	try{
			itsFileReader.open(fname);
			itsFileReader.exceptions(ifstream::failbit | ifstream::badbit);
		}
		catch(ios_base::failure& e){
			cout << "Opening file failed" <<endl;
			cout << e.what() <<endl;
		}
}

string TextFileReader::read(){
	string content;
	string line;
	while( !itsFileReader.eof()){
		getline( itsFileReader, line);
		content += line;
		content += "\n";
	}
	return content;
}
