/*
 * TextFileReader.h
 *
 *  Created on: 15 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_INPUT_TEXTFILEREADER_H_
#define V12_INPUT_TEXTFILEREADER_H_

#include <iostream>
#include <fstream>

#include "../Interfaces/InputChannel.h"

class TextFileReader : public InputChannel {
protected:
	ifstream itsFileReader;
public:
	TextFileReader(){};
	virtual ~TextFileReader(){};
	void close() override;
	string read() override;
	void openForReading( string );
};

#endif /* V12_INPUT_TEXTFILEREADER_H_ */
