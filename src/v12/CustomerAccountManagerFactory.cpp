#include "stdafx.h"
#include "CustomerAccountManagerFactory.h"
#include "Interfaces/ICustomerAccountManager.h"
#include "Domain/Account.h"

ICustomerAccountManager* CustomerAccountManagerFactory::itsCustomerAccountMgr = NULL;

unordered_map<string, Customer*> CustomerAccountManagerFactory::CustomerAccountMgr::getCustomers() {
	return customers;
}

unordered_map<int, Account*> CustomerAccountManagerFactory::CustomerAccountMgr::getAccounts() {
	return accounts;
}

Customer* CustomerAccountManagerFactory::CustomerAccountMgr::getCustomer(string cId) {
	unordered_map< string, Customer*>::iterator got = customers.find(cId);

	if (got == customers.end()) {
		return NULL;
	}
	else {
		Customer* cust = got->second;
		return cust;
	}
}

Account* CustomerAccountManagerFactory::CustomerAccountMgr::getAccount(int accId) {
	unordered_map< int, Account*>::iterator got = accounts.find(accId);

	if (got == accounts.end()) {
		return NULL;
	}
	else {
		Account* acc = got->second;
		return acc;
	}
}

void CustomerAccountManagerFactory::CustomerAccountMgr::addCustomer(string cId, Customer* cust) {
	customers.insert(make_pair(cId, cust));
}

void CustomerAccountManagerFactory::CustomerAccountMgr::addAccount(Account* acc) {
	int accId = acc->getAccountId();
	accounts.insert(make_pair(accId, acc));
}
