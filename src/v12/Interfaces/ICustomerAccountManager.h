#ifndef __CUSTOMER_ACCOUNT_MANAGER_H
#define	__CUSTOMER_ACCOUNT_MANAGER_H

#include <string>
using namespace std;

class Account;
class Customer;

class ICustomerAccountManager
{
public:
	virtual	unordered_map<string, Customer*> getCustomers() = 0;
	virtual	unordered_map<int, Account*> getAccounts() = 0;
	virtual	Customer* getCustomer(string) = 0;
	virtual	Account* getAccount(int) = 0;
	virtual	void addCustomer(string, Customer*) = 0;
	virtual	void addAccount(Account*) = 0;
	virtual ~ICustomerAccountManager() {};
};


#endif // !__CUSTOMER_ACCOUNT_MANAGER_H

#pragma once
