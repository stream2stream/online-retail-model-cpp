/*
 * BusinessObjectRenderer.h
 *
 *  Created on: 25 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_INTERFACES_BUSINESSOBJECTRENDERER_H_
#define V12_INTERFACES_BUSINESSOBJECTRENDERER_H_

#include <boost/any.hpp>

class BusinessObjectRenderer {
public:
	virtual ~BusinessObjectRenderer(){};
	virtual void display(boost::any obj) = 0;
};

#endif /* V12_INTERFACES_BUSINESSOBJECTRENDERER_H_ */
