/*
 * InputChannel.h
 *
 *  Created on: 10 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_INPUTCHANNEL_H_
#define V12_INPUTCHANNEL_H_

#include <string>
using namespace std;

class InputChannel {
public:
	virtual ~InputChannel(){};
	virtual string read()=0;
	virtual void close()=0;
};

#endif /* V12_INPUTCHANNEL_H_ */
