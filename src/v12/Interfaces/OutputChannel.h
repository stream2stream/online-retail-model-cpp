/*
 * OutputChannel.h
 *
 *  Created on: 15 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUTCHANNEL_H_
#define V12_OUTPUTCHANNEL_H_

#include <string>
using namespace std;

class OutputChannel {
public:
	virtual ~OutputChannel(){};
	virtual void openForWriting( string ) =0;
	virtual void write( string ) = 0;
	virtual void close() = 0;
};

#endif /* V12_OUTPUTCHANNEL_H_ */
