/*
 * Warehouse.h
 *
 *  Created on: 9 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_WAREHOUSE_H_
#define V12_WAREHOUSE_H_
#include <unordered_map>

#include "Domain/Product.h"
using namespace std;

class Warehouse {
private:
	Warehouse(){};
	static Warehouse* m_pInstance;
	unordered_map<string, Product*> products;
	void addProducts();

public:
	static Warehouse* getInstance();
	unordered_map<string, Product*> getProducts();
	Product* getProduct( string );
	virtual ~Warehouse(){};
};

#endif /* V12_WAREHOUSE_H_ */
