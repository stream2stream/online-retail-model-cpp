/*
 * KeyboardHandler.h
 *
 *  Created on: 29 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_KEYBOARDHANDLER_H_
#define V12_KEYBOARDHANDLER_H_
#include <string>
#include <unordered_map>

#include "RetailShop.h"

using namespace std;
class KeyboardHandler{
private:

	static KeyboardHandler* m_pInstance;
	static string const stdPrompt ;
	static string const quitCmd;
	static string promptCommands;
	unordered_map<string, function<void(RetailShop*)> > commands;
public:
	KeyboardHandler(){};
	static KeyboardHandler* getInstance();
	string readString();
	virtual ~KeyboardHandler(){};
	void operator()();
	void addCommand( string, function<void( RetailShop*)> );
};

#endif /* V12_KEYBOARDHANDLER_H_ */
