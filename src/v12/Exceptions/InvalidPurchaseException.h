/*
 * InvalidPurchaseException.h
 *
 *  Created on: 17 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_EXCEPTIONS_INVALIDPURCHASEEXCEPTION_H_
#define V12_EXCEPTIONS_INVALIDPURCHASEEXCEPTION_H_

#include <string>
using namespace std;

class InvalidPurchaseException : public exception {
private:
	string msg;
public:
	explicit InvalidPurchaseException( const string& message ): msg(message){};
	virtual const char* what() const throw(){
		return msg.c_str();
	};
	~InvalidPurchaseException() throw(){};
};

#endif /* V12_EXCEPTIONS_INVALIDPURCHASEEXCEPTION_H_ */
