/*
 * NotEnoughStockException.h
 *
 *  Created on: 17 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_EXCEPTIONS_NOTENOUGHSTOCKEXCEPTION_H_
#define V12_EXCEPTIONS_NOTENOUGHSTOCKEXCEPTION_H_

#include <exception>
#include <string>
using namespace std;

class NotEnoughStockException: public exception {
private:
	string msg;
public:
	explicit NotEnoughStockException( const string& message ): msg(message){};
	virtual const char* what() const throw(){
		return msg.c_str();
	};
	~NotEnoughStockException() throw(){};
};

#endif /* V12_EXCEPTIONS_NOTENOUGHSTOCKEXCEPTION_H_ */
