//============================================================================
// Name        : HelloWorld.cpp
// Author      : Martina
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <functional>

#include "KeyboardHandler.h"
#include "ShutdownHandler.h"
#include "CustomerAccountManagerFactory.h"
using namespace std;

int main() 
{
	ShutdownHandler* sdh = new ShutdownHandler();
	KeyboardHandler* kh = KeyboardHandler::getInstance();
	kh->addCommand("save", [] (RetailShop* shop){
		cout<<"Save command called"<<endl;
		shop->test_serialization_writer_all_items();
	});

	kh->addCommand("load", [] (RetailShop* shop){
		cout<<"Load command called"<<endl;
		shop->test_serialization_reader_all_items();
	});

	kh->addCommand("create", [] (RetailShop* shop){
		cout << "Create command called" << endl;
		shop->bootstrap_products_items();
		shop->bootstrap_customers_account();
		shop->bootstrap_orders_accounts_items();
	});

	kh->addCommand("json to textfile", [](RetailShop* shop) {
		cout << "Write JSON structure to textfile command called" << endl;
	});

	kh->addCommand("xml to textfile", [](RetailShop* shop) {
		cout << "Write XML structure to textfile command called" << endl;
	});

	kh->addCommand("serialize customer", [](RetailShop* shop) {
		cout << "Serialize a Customer object to a file command called" << endl;
	});

	kh->addCommand("deserialize customer", [](RetailShop* shop) {
		cout << "Deserialize a Customer object from a file command called" << endl;
	});

	sdh->waitForShutdownCommand();
	return 0;
};
