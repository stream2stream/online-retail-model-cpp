#ifndef __CUSTOMER_ACCOUNT_MANAGER_FACTORY_H
#define __CUSTOMER_ACCOUNT_MANAGER_FACTORY_H
#pragma once

#include <unordered_map>
#include "Interfaces/ICustomerAccountManager.h"
//#include "CustomerAccountMgr.h"
using namespace std;

class CustomerAccountManagerFactory
{
private:
	class CustomerAccountMgr : public ICustomerAccountManager
	{
	public:
		unordered_map<string, Customer*> getCustomers();
		unordered_map<int, Account*> getAccounts();
		Customer* getCustomer(string);
		Account* getAccount(int);
		void addCustomer(string, Customer*);
		void addAccount(Account*);
		virtual ~CustomerAccountMgr() {};	
	private:
		unordered_map<string, Customer*> customers;
		unordered_map<int, Account*> accounts;

	};

	static ICustomerAccountManager* itsCustomerAccountMgr;

public:
	ICustomerAccountManager*  getCustomerAccountMgr()
	{
		//++ todo implement factory code here
		return itsCustomerAccountMgr;
	}

	void setCustomerAccountMgr(ICustomerAccountManager* caMgr)
	{
		itsCustomerAccountMgr = caMgr;
	}
};



#endif // !__CUSTOMER_ACCOUNT_MANAGER_FACTORY_H

