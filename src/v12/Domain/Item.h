/*
 * Item.h
 *
 *  Created on: 2 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_DOMAIN_ITEM_H_
#define V12_DOMAIN_ITEM_H_

#include <boost/serialization/access.hpp>

class Order;
class Product;
class Item {
private:
	friend class boost::serialization::access;
	template <class Archive>
	void serialize( Archive& ar, const unsigned int version );
	template <class Archive>
	void save( Archive & ar, const unsigned int version ) const;
	template <class Archive>
	void load( Archive & ar, const unsigned int version );
	double item_cost;
	int unique_id;
	Product* itsProduct;
	int quantity;
	Order* itsOrder;
public:
	Item(){};
	Item( Product *, double, int, int );
	virtual ~Item(){};

	double getTotalCost(){
		return quantity * item_cost;
	}

	Product* getItsProduct(){
		return itsProduct;
	}

	double getItemCost() const {
		return item_cost;
	}

	void setItemCost(double itemCost) {
		item_cost = itemCost;
	}

	int getQuantity(){
		return quantity;
	}

	void setQuantity(int quantity) {
		this -> quantity = quantity;
	}

	int getUniqueId(){
		return unique_id;
	}

	void setUniqueId(int uniqueId) {
		unique_id = uniqueId;
	}

	void setOrder(Order* anOrder){
		itsOrder = anOrder;
	}

	Order* getOrder(){
		return itsOrder;
	}
};

#endif /* V12_DOMAIN_ITEM_H_ */
