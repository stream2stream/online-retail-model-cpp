/*
 * Account.cpp
 *
 *  Created on: 2 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "Account.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/split_member.hpp>
#include <boost/lexical_cast.hpp>
#include <string>


#include "Customer.h"
#include "Order.h"

template <class Archive>
void Account::serialize(Archive &ar, unsigned int file_version){
	boost::serialization::split_member( ar, *this, file_version);
}

template <class Archive>
void Account::save(Archive &ar, unsigned int version) const {
	ar << account_id;
	ar << date_opened;
	ar << orders;
}

template <class Archive>
void Account::load(Archive &ar, unsigned int version){
	ar >> account_id;
	ar >> date_opened;
	ar >> orders;
	for (auto it = orders.begin(); it != orders.end(); ++it)
	{
		Order* order = it->second;
		order->setItsAccount( this );
	}
}

template void Account::serialize<boost::archive::text_oarchive>(
		boost::archive::text_oarchive & ar,
		const unsigned int file_version
);

template void Account::serialize<boost::archive::text_iarchive>(
		boost::archive::text_iarchive & ar,
		const unsigned int file_version
);

Account::Account(){
	this -> itsDisplayHandler = new Account::DisplayHandler();
}

Account::Account(Customer* customer, int accountId) {
	this -> itsCustomer = customer;
	this -> account_id = accountId;
	this -> itsDisplayHandler = new Account::DisplayHandler();
	time(&this->date_opened);
}

void Account::addOrder( Order* order ) {
	int orderId = order->getOrderNo();
	orders.insert( make_pair( orderId, order) );
}

void Account::DisplayHandler::display(boost::any obj){
	Account* acc = boost::any_cast<Account*>(obj);
	cout << "Account \t Account ID \t Date Created" <<endl;
	time_t date = acc->date_opened;
	//time(&date);
	struct tm *timeinfo = localtime(&date);
	cout << "\t\t" << acc->account_id << "\t\t" << timeinfo->tm_mday << "-" <<  1 + timeinfo->tm_mon << "-" << 1900 + timeinfo->tm_year << endl;
}
BOOST_CLASS_EXPORT(Account)
