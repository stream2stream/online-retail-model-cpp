/*
 * Customer.h
 *
 *  Created on: 3 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_DOMAIN_CUSTOMER_H_
#define V12_DOMAIN_CUSTOMER_H_
#include <ctime>
#include <iostream>
#include <string>
#include <boost/serialization/access.hpp>

#include "../Interfaces/BusinessObjectRenderer.h"
using namespace std;

class Account;
class Customer {
private:
	friend class boost::serialization::access;
	template <class Archive>
	void serialize( Archive & ar, const unsigned int version );
	template <class Archive>
	void save( Archive & ar, const unsigned int version ) const;
	template <class Archive>
	void load( Archive & ar, const unsigned int version );
	time_t dob;
	string address;
	string fname;
	string lname;
	Account* itsAccount;
	class DisplayHandler : public BusinessObjectRenderer{
		void display(boost::any) override;
	};
	DisplayHandler* itsDisplayHandler;

	public:
	Customer();
	Customer( string, string, string, time_t );
	virtual ~Customer(){};

	const string& getAddress() const {
		return address;
	}

	void setAddress(const string& address) {
		this->address = address;
	}

	time_t getDob() const {
		return dob;
	}

	void setDob(time_t dob) {
		this->dob = dob;
	}

	const string& getFname() const {
		return fname;
	}

	void setFname(const string& fname) {
		this->fname = fname;
	}


	const string& getLname() const {
		return lname;
	}

	void setLname(const string& lname) {
		this->lname = lname;
	}

	void setItsAccount( Account* acc);

	Account* getItsAccount();

	BusinessObjectRenderer* getRenderer(){
		return itsDisplayHandler;
	}
};

#endif /* V12_DOMAIN_CUSTOMER_H_ */
