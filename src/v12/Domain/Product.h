/*
 * Product.h
 *
 *  Created on: 4 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_DOMAIN_PRODUCT_H_
#define V12_DOMAIN_PRODUCT_H_
#include <string>
#include <unordered_map>
#include <boost/serialization/access.hpp>
using namespace std;

class Item;
class Product {
private:
	friend class boost::serialization::access;
	template <class Archive>
	void serialize( Archive & ar, const unsigned int version );
	template <class Archive>
	void save( Archive & ar, const unsigned int version ) const;
	template <class Archive>
	void load( Archive & ar, const unsigned int version );
	string description;
	double price;
	string product_code;
	int quantity;
	unordered_map<int, Item*> items;

public:
	Product(){};
	Product( string, double, string, int );
	virtual ~Product(){};

	void decreaseStock();

	void decreaseStock( int );

	Item* getItem(int itemId);

	void addItem( Item* anItem );

	unordered_map<int, Item*> getItems(){
		return items;
	}

	string getDescription(){
		return description;
	}

	void setDescription(string description) {
		this->description = description;
	}

	double getPrice(){
		return price;
	}

	void setPrice(double price) {
		this->price = price;
	}

	string getProductCode(){
		return product_code;
	}

	void setProductCode(string productCode) {
		product_code = productCode;
	}

	int getQuantity(){
		return quantity;
	}

	void setQuantity(int quantity) {
		this->quantity = quantity;
	}

	Item* purchaseItem(int);
};

#endif /* V12_DOMAIN_PRODUCT_H_ */
