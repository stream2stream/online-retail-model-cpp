/*
 * Item.cpp
 *
 *  Created on: 2 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "Item.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/split_member.hpp>

#include "Order.h"
#include "Product.h"

template <class Archive>
void Item::serialize( Archive& ar, const unsigned int file_version ){
	boost::serialization::split_member( ar, *this, file_version);
}

template <class Archive>
void Item::save(Archive &ar, unsigned int version) const {
	ar << item_cost;
	ar << unique_id;
	ar << itsProduct;
	ar << quantity;
}

template <class Archive>
void Item::load(Archive &ar, unsigned int version){
	ar >> item_cost;
	ar >> unique_id;
	ar >> itsProduct;
	ar >> quantity;
	Product* prod = this->itsProduct;
	prod->addItem(this);
}

template void Item::serialize<boost::archive::text_oarchive>(
		boost::archive::text_oarchive & ar,
		const unsigned int file_version
);

template void Item::serialize<boost::archive::text_iarchive>(
		boost::archive::text_iarchive & ar,
		const unsigned int file_version
);

Item::Item( Product* product, double item_cost, int quantity, int unique_id ){
	this -> itsProduct = product;
	this -> item_cost = item_cost;
	this -> unique_id = unique_id;
	this->quantity = quantity;
}
BOOST_CLASS_EXPORT(Item);
