/*
 * Account.h
 *
 *  Created on: 2 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_DOMAIN_ACCOUNT_H_
#define V12_DOMAIN_ACCOUNT_H_

#include <ctime>
#include <boost/serialization/access.hpp>
#include <unordered_map>

#include "../Interfaces/BusinessObjectRenderer.h"
using namespace std;

class Order;
class Customer;
class Account {
private:
	friend class boost::serialization::access;
	template <class Archive>
	void serialize( Archive & ar, const unsigned int file_version );
	template <class Archive>
	void save( Archive & ar, const unsigned int version ) const;
	template <class Archive>
	void load( Archive & ar, const unsigned int version );
	int account_id;
	time_t date_opened;
	unordered_map<int, Order*> orders;
	Customer* itsCustomer;
	class DisplayHandler : public BusinessObjectRenderer{
		void display(boost::any) override;
	};
	DisplayHandler* itsDisplayHandler;

	public:

	Account();
	Account( Customer*, int );
	virtual ~Account(){};

	int getAccountId() {
		return account_id;
	}

	time_t getDateOpened( ) {
		return date_opened;
	}

	void setDateOpened(time_t dateOpened) {
		date_opened = dateOpened;
	}

	void addOrder( Order* );

	Customer* getItsCustomer(){
		return itsCustomer;
	}

	void setItsCustomer( Customer* cc){
		itsCustomer = cc;;
	}

	unordered_map<int, Order*> getOrders(){
		return orders;
	}

	BusinessObjectRenderer* getRenderer(){
		return itsDisplayHandler;
	}
};

#endif /* V12_DOMAIN_ACCOUNT_H_ */
