/*
 * Product.cpp
 *
 *  Created on: 4 May 2018
 *      Author: martinayusuf
 */


#include "stdafx.h"
#include "Product.h"

#include <boost/serialization/unordered_map.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/split_member.hpp>

#include "Item.h"
#include "../Exceptions/InvalidPurchaseException.h"
#include "../Exceptions/NotEnoughStockException.h"

template <class Archive>
void Product::serialize( Archive & ar, const unsigned int file_version ){
	boost::serialization::split_member( ar, *this, file_version);
}

template <class Archive>
void Product::save(Archive &ar, unsigned int version) const {
	ar << description;
	ar << price;
	ar << product_code;
	ar << quantity;
}

template <class Archive>
void Product::load(Archive &ar, unsigned int version){
	ar >> description;
	ar >> price;
	ar >> product_code;
	ar >> quantity;
}

template void Product::serialize<boost::archive::text_oarchive>(
		boost::archive::text_oarchive & ar,
		const unsigned int file_version
);

template void Product::serialize<boost::archive::text_iarchive>(
		boost::archive::text_iarchive & ar,
		const unsigned int file_version
);

Product::Product(string description, double price, string product_code, int quantity){
	this -> description = description;
	this -> price = price;
	this -> product_code = product_code;
	this -> quantity = quantity;
}

void Product::addItem( Item* anItem){
	int unique_id = anItem -> getUniqueId();
	items.insert( make_pair( unique_id, anItem) );
}

Item* Product::getItem(int itemId){
	unordered_map< int,Item*>::iterator got = items.find (itemId);
	if ( got == items.end() ){
		return NULL;
	}
	else {
		Item* item = got->second;
		return item;
	}
}

void Product::decreaseStock(){
	if (quantity < 0) {
		throw new NotEnoughStockException( description + "is out of stock");
	}
	quantity --;
}

void Product::decreaseStock( int amt){
	if (quantity - amt < 0) {
		throw new NotEnoughStockException( description + " is out of stock");
	}
	quantity -= amt;
}

Item* Product::purchaseItem( int qty ){
	try{
		decreaseStock(qty);
	}
	catch( NotEnoughStockException* e ){
		cout << e->what() << endl;
		throw new InvalidPurchaseException( " Purchase unsuccessful " );
	}
	return new Item(this, this->price, qty, 1);
}
BOOST_CLASS_EXPORT(Product);
