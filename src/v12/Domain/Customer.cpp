/*
 * Customer.cpp
 *
 *  Created on: 3 May 2018
 *      Author: martinayusuf
 */


#include "stdafx.h"
#include "Customer.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/split_member.hpp>

#include "Account.h"

template <class Archive>
void Customer::serialize( Archive & ar, const unsigned int file_version ){
	boost::serialization::split_member( ar, *this, file_version);
}

template <class Archive>
void Customer::save(Archive &ar, unsigned int version) const {
	ar << dob;
	ar << address;
	ar << fname;
	ar << lname;
	ar << itsAccount;
}

template <class Archive>
void Customer::load(Archive &ar, unsigned int version){
	ar >> dob;
	ar >> address;
	ar >> fname;
	ar >> lname;
	ar >> itsAccount;
	Account* acc = this -> itsAccount;
	acc -> setItsCustomer(this);
}

template void Customer::serialize<boost::archive::text_oarchive>(
		boost::archive::text_oarchive & ar,
		const unsigned int file_version
);

template void Customer::serialize<boost::archive::text_iarchive>(
		boost::archive::text_iarchive & ar,
		const unsigned int file_version
);

Customer::Customer( string fname, string lname, string address, time_t dob) {
	this -> fname = fname;
	this -> lname = lname;
	this -> address = address;
	this -> dob = dob;
	this -> itsDisplayHandler = new Customer::DisplayHandler();
}

Customer::Customer(){
	this -> itsDisplayHandler = new Customer::DisplayHandler();
}

Account* Customer::getItsAccount() {
	return  this -> itsAccount;
}

void Customer::setItsAccount( Account* acc) {
	this -> itsAccount = acc;
}

void Customer::DisplayHandler::display(boost::any obj){
	Customer* cust = boost::any_cast<Customer*>(obj);
	time_t date = cust->dob;
	tm *ltm = localtime(&date);
	cout<< "Customer\tFIRST NAME\tLAST NAME\tDOB\t\tADDRESS" <<endl;
	cout<<"\t\t" << cust->fname << "\t\t" << cust->lname << "\t\t" << ltm->tm_mday << "-" <<  1 + ltm->tm_mon << "-" << 1900 + ltm->tm_year  << "\t"<< cust->address<<endl;
}
BOOST_CLASS_EXPORT(Customer);
