/*
 * Order.cpp
 *
 *  Created on: 3 May 2018
 *      Author: martinayusuf
 */


#include "stdafx.h"
#include "Order.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/split_member.hpp>

#include "Account.h"
#include "Item.h"
template <class Archive>
void Order::serialize( Archive& ar, const unsigned int file_version ){
	boost::serialization::split_member( ar, *this, file_version);
}

template <class Archive>
void Order::save(Archive &ar, unsigned int version) const {
	ar << order_no;
	ar << order_date;
	ar << total_cost;
	ar << item;
}

template <class Archive>
void Order::load(Archive &ar, unsigned int version){
	ar >> order_no;
	ar >> order_date;
	ar >> total_cost;
	ar >> item;
	Item* item = this -> item;
	item -> setOrder(this);
}

template void Order::serialize<boost::archive::text_oarchive>(
		boost::archive::text_oarchive & ar,
		const unsigned int file_version
);

template void Order::serialize<boost::archive::text_iarchive>(
		boost::archive::text_iarchive & ar,
		const unsigned int file_version
);

Order::Order( Account* account, int order_no, time_t order_date){
	this -> itsAccount = account;
	this -> order_no = order_no;
	this -> order_date = order_date;
}

void Order::addItem(Item* anItem){
	this -> item = anItem;
}


double Order::getTotalCost(){
	double result = 0.0;
	//++ todo
	// calculate the total cost of the order by iteratig over all items
	// make sure your solution isn't rigid, you should be able to alter
	// formulea without having to rebuild/redesign the Order class

	return result;
}
BOOST_CLASS_EXPORT(Order);
