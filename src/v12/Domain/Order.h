/*
 * Order.h
 *
 *  Created on: 3 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_DOMAIN_ORDER_H_
#define V12_DOMAIN_ORDER_H_
#include <ctime>
#include <boost/serialization/access.hpp>

class Item;
class Account;
class Order {
private:
	friend class boost::serialization::access;
	template <class Archive>
	void serialize( Archive& ar, const unsigned int version );
	template <class Archive>
	void save( Archive & ar, const unsigned int version ) const;
	template <class Archive>
	void load( Archive & ar, const unsigned int version );
	int order_no;
	time_t order_date;
	double total_cost;
	Item* item;
	Account* itsAccount;
public:
	Order(){};
	Order( Account*, int, time_t);
	virtual ~Order(){};

	void addItem( Item* );

	time_t getOrderDate() {
		return order_date;
	}

	int getOrderNo() {
		return order_no;
	}

	double getTotalCost();

	void setOrderDate(time_t orderDate) {
		order_date = orderDate;
	}

	void setOrderNo(int orderNo) {
		order_no = orderNo;
	}

	Item* getItems(){
		return item;
	}

	void setItsAccount(Account* acc){
		itsAccount = acc;
	}
};

#endif /* V12_DOMAIN_ORDER_H_ */
