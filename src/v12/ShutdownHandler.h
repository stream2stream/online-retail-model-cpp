/*
 * ShutdownHandler.h
 *
 *  Created on: 29 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_SHUTDOWNHANDLER_H_
#define V12_SHUTDOWNHANDLER_H_

class ShutdownHandler {
public:
	ShutdownHandler(){};
	virtual ~ShutdownHandler(){};
	void waitForShutdownCommand();
};

#endif /* V12_SHUTDOWNHANDLER_H_ */
