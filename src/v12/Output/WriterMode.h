/*
 * WriterMode.h
 *
 *  Created on: 10 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUT_WRITERMODE_H_
#define V12_OUTPUT_WRITERMODE_H_

enum class WriterMode {
	JSON_WRITER,
	XML_WRITER
};

#endif /* V12_WRITERMODE_H_ */
