/*
 * TextFileWriter.h
 *
 *  Created on: 15 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUT_TEXTFILEWRITER_H_
#define V12_OUTPUT_TEXTFILEWRITER_H_

#include "../Interfaces/OutputChannel.h"
#include <iostream>
#include <fstream>


class TextFileWriter : public OutputChannel{
protected:
	ofstream itsPrintWriter;
public:
	TextFileWriter(){};
	virtual ~TextFileWriter(){};
	void openForWriting( string ) override;
	void write ( string ) override;
	void close() override;
};

#endif /* V12_OUTPUT_TEXTFILEWRITER_H_ */
