/*
 * XMLFormatter.h
 *
 *  Created on: 9 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUT_XMLFORMATTER_H_
#define V12_OUTPUT_XMLFORMATTER_H_
#include <string>

#include "StreamFormatter.h"

class XMLFormatter: public StreamFormatter {
public:
	XMLFormatter(){};
	virtual ~XMLFormatter(){};
	string format( Account* ) override;
	string format( Customer* ) override;
};

#endif /* V12_OUTPUT_XMLFORMATTER_H_ */
