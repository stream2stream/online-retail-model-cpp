/*
 * FormatFactory.h
 *
 *  Created on: 10 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUT_FORMATFACTORY_H_
#define V12_OUTPUT_FORMATFACTORY_H_
#include "WriterMode.h"
#include "XMLFormatter.h"

class FormatFactory {
private:
	static XMLFormatter* xmlFormatter;
public:
	FormatFactory(){};
	virtual ~FormatFactory(){};
	static StreamFormatter* getFormatter( WriterMode );
};

#endif /* V12_FORMATFACTORY_H_ */
