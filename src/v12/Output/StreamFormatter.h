/*
 * StreamFormatter.h
 *
 *  Created on: 10 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUT_STREAMFORMATTER_H_
#define V12_OUTPUT_STREAMFORMATTER_H_


#include "../Domain/Account.h"
#include "../Domain/Customer.h"
using namespace std;

class StreamFormatter {
public:
	virtual ~StreamFormatter(){};
	virtual string format( Account* )=0;
	virtual string format( Customer* )=0;
};

#endif /* V12_STREAMFORMATTER_H_ */
