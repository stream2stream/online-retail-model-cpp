/*
 * SerializedFileWriter.h
 *
 *  Created on: 18 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUT_SERIALIZEDFILEWRITER_H_
#define V12_OUTPUT_SERIALIZEDFILEWRITER_H_

#include <string>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/unordered_map.hpp>

#include "TextFileWriter.h"

using namespace std;

class SerializedFileWriter : public TextFileWriter{
public:
	SerializedFileWriter(){};
	virtual ~SerializedFileWriter(){};

	template <class T>
	void writeSerializedData( T obj){
		boost::archive::text_oarchive oa(itsPrintWriter);
			oa << obj;
	}

	template<typename Map>
	void writeSerializedMap(const Map map, bool additionalParam = false) {
		boost::archive::text_oarchive oa(itsPrintWriter);
		oa << map.size();
	    for (auto it = map.begin(); it != map.end(); ++it){
	        oa<<it->first<< it->second;
	    }
	}
};

#endif /* V12_OUTPUT_SERIALIZEDFILEWRITER_H_ */
