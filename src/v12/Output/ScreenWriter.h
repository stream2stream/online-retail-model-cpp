/*
 * ScreenWriter.h
 *
 *  Created on: 4 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUT_SCREENWRITER_H_
#define V12_OUTPUT_SCREENWRITER_H_
#include <string>
#include <iostream>
#include "../Interfaces/OutputChannel.h"
using namespace std;

class ScreenWriter : public OutputChannel {
public:
	ScreenWriter(){};
	virtual ~ScreenWriter(){};
	void openForWriting( string fname) {};
	void write( string stream ){
		cout << stream << endl;
	}
	void close(){};
};

#endif /* V12_OUTPUT_SCREENWRITER_H_ */
