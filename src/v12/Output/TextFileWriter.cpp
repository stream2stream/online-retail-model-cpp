/*
 * TextFileWriter.cpp
 *
 *  Created on: 15 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "TextFileWriter.h"

void TextFileWriter::close() {
	try{
		itsPrintWriter.close();
	}
	catch (ios_base::failure& e)
	{
		cout << e.what() << endl;
	}
}

void TextFileWriter::write( string dataStream){
	itsPrintWriter << dataStream;
}

void TextFileWriter::openForWriting( string fname){
	try{
		itsPrintWriter.open(fname);
		itsPrintWriter.exceptions( ofstream::failbit | ofstream::badbit );
	}
	catch(ios_base::failure& e){
		cout << "Opening file failed" <<endl;
		cout << e.what() <<endl;
	}
}

