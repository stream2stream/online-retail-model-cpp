/*
 * Writer.h
 *
 *  Created on: 4 May 2018
 *      Author: martinayusuf
 */

#ifndef V12_OUTPUT_WRITER_H_
#define V12_OUTPUT_WRITER_H_
#include "ScreenWriter.h"
#include "SerializedFileWriter.h"
#include "TextFileWriter.h"

class Writer {
private:
	static ScreenWriter* screenWriter;
	//++ todo
public:
	Writer(){};
	static OutputChannel* getScreenWriter();
	//++ todo
	virtual ~Writer(){};
};

#endif /* V12_OUTPUT_WRITER_H_ */
