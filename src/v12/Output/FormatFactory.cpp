/*
 * FormatFactory.cpp
 *
 *  Created on: 10 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "FormatFactory.h"

#include "../Domain/Account.h"

XMLFormatter* FormatFactory::xmlFormatter = new XMLFormatter();

StreamFormatter* FormatFactory::getFormatter( WriterMode mode){
	StreamFormatter* result = NULL;
	if ( mode == WriterMode::XML_WRITER ){
		result = xmlFormatter;
	}
	return result;
}
