/*
 * XMLFormatter.cpp
 *
 *  Created on: 9 May 2018
 *      Author: martinayusuf
 */

#include "stdafx.h"
#include "XMLFormatter.h"

string XMLFormatter::format( Account* acc ){
	time_t date_opened =  acc->getDateOpened();
	char* date_char = ctime(&date_opened);
	string result = "<account>";
	result += "\n\t<datecreated>";
	result += date_char;
	result += "</datecreated>";
	result += "\n</account>";
	return result;
}

string XMLFormatter::format( Customer* cust){
	time_t date =  cust->getDob();
	char* date_char = ctime(&date);
	string result = "<customer>";
	result += "\n\t<fname>" + cust->getFname() + "</fname>";
	result += "\n\t<lname>" + cust->getLname() + "</lname>";
	result += "\n\t<dob>";
	result +=  date_char;
	result +=  "</dob>";
	result += "\n\t<address>" + cust->getAddress() + "</address>";
	result += "\n</customer>";

	return result;
}
