/*
 * Warehouse.cpp
 *
 *  Created on: 9 May 2018
 *      Author: martinayusuf
 */
#include "stdafx.h"
#include "Warehouse.h"

#include "Domain/Item.h"

Warehouse* Warehouse::m_pInstance = NULL;

Warehouse* Warehouse::getInstance() {
	if ( !m_pInstance ){
		m_pInstance = new Warehouse;
		m_pInstance->addProducts();
	}
	return m_pInstance;
}

void Warehouse::addProducts(){
	Product* prod = new Product("JVC DVD Player", 179.99, "JDF-001", 5);
	m_pInstance->products.insert( make_pair("JDF-001", prod) );
	prod = new Product("Sony MIDI Amplifier", 203.45, "SMA-001", 3);
	m_pInstance->products.insert(make_pair("SMA-001", prod));
	prod = new Product("Lenovo All In One 302", 312.00, "LAIO-302",7);
	m_pInstance->products.insert(make_pair( "LAIO-302", prod ));
}

unordered_map<string,Product*> Warehouse::getProducts(){
	return m_pInstance->products;
}

Product* Warehouse::getProduct( string pid){

	  unordered_map< string,Product*>::iterator got = m_pInstance->products.find (pid);

	  if ( got == m_pInstance->products.end() ){
		  return NULL;
	  }
	  else {
		  Product* prod = got->second;
		  return prod;
	  }
}
